# ReliefApps Test Backend

## Steps

1. `npm install`

2. `npm start`

## Routes

### History

- `GET localhost:8000/history`

---

- `POST localhost:8000/history`

### Bookmark

- `GET localhost:8000/bookmark`

---

- `POST localhost:8000/bookmark`
