const express = require('express');
const cors = require('cors');

const app = express();
const port = 8000;
const historyRoute = require('./routes/history.js');
const bookmarkRoute = require('./routes/bookmark.js');

app.use(cors());
app.use(express.json());

app.use('/history', historyRoute);
app.use('/bookmark', bookmarkRoute);

app.listen(port, () => {
    console.log(`Back-end listening to http://localhost:${port}`);
});
