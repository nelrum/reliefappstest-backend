const express = require('express');
const router = express.Router();

var video = require('./../model/videoModel').Video;

var bookmark = [];

router.get('/', (req, res) => {
    res.json(bookmark);
});

router.post('/', (req, res) => {
    var newVideo = new video(req.body["id"]);

    bookmark.unshift(newVideo);
    res.json({ status: 'Sent', bookmark });
});

module.exports = router;