const express = require('express');
const router = express.Router();

var video = require('./../model/videoModel').Video;

var history = [];

router.get('/', (req, res) => {
    res.contentType('application/json');
    res.json(history);
});

router.post('/', (req, res) => {
    var newVideo = new video(req.body["id"]);

    history.unshift(newVideo);
    res.json({ status: 'Sent', history });
});

module.exports = router;